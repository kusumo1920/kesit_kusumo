package com.kusumo1920.meal0inventory.controller;

import com.kusumo1920.meal0inventory.service.InventoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/inventory")
@RequiredArgsConstructor
public class InventoryController {
    private final InventoryService inventoryService;

    @GetMapping("/check-stock")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<IsInStockResponsePayload> isInStock(
            @RequestParam("skuCode") List<String> skuCodes
            ) {
        List<InventoryResponseItem> inStocks = inventoryService.isInStock(skuCodes);

        IsInStockResponsePayload responsePayload = IsInStockResponsePayload.builder()
                .code(200)
                .message("Successfully checked inventory stock.")
                .data(inStocks)
                .build();

        return ResponseEntity.ok(responsePayload);
    }
}
