package com.kusumo1920.meal0inventory.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InventoryResponseItem {
    private String skuCode;
    private boolean isInStock;
}
