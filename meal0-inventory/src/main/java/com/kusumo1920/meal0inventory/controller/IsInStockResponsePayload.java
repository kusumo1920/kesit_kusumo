package com.kusumo1920.meal0inventory.controller;

import com.kusumo1920.meal0inventory.controller.common.ResponseWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class IsInStockResponsePayload extends ResponseWrapper {
    private List<InventoryResponseItem> data;
}
