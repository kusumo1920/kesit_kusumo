package com.kusumo1920.meal0inventory.service;

import com.kusumo1920.meal0inventory.controller.InventoryResponseItem;
import com.kusumo1920.meal0inventory.repository.InventoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class InventoryService {
    private final InventoryRepository inventoryRepository;

    @Transactional(readOnly = true)
    public List<InventoryResponseItem> isInStock(List<String> skuCodes) {
        return inventoryRepository.findBySkuCodeIn(skuCodes).stream()
                .map(inventory ->
                    InventoryResponseItem.builder()
                            .skuCode(inventory.getSkuCode())
                            .isInStock(inventory.getQty() > 0)
                            .build()
                ).toList();
    }
}
