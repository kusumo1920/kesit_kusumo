package com.kusumo1920.meal0inventory.dao;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_inventory")
public class Inventory {
    @Id
    @GeneratedValue
    private Integer id;
    @Column(unique = true)
    private String skuCode;
    private Integer qty;
}
