package com.kusumo1920.meal0inventory;

import com.kusumo1920.meal0inventory.dao.Inventory;
import com.kusumo1920.meal0inventory.repository.InventoryRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Meal0InventoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(Meal0InventoryApplication.class, args);
	}

	@Bean
	public CommandLineRunner loadData(InventoryRepository inventoryRepository) {
		return args -> {
			Inventory inventory1 = Inventory.builder()
					.skuCode("rujak_manis")
					.qty(100)
					.build();

			Inventory inventory2 = Inventory.builder()
					.skuCode("pecel_madiun")
					.qty(0)
					.build();

			try {
				inventoryRepository.save(inventory1);
				inventoryRepository.save(inventory2);
			} catch (Exception ignored) {}

		};
	}

}
