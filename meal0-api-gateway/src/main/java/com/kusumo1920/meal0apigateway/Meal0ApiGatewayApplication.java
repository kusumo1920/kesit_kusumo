package com.kusumo1920.meal0apigateway;

import com.kusumo1920.meal0apigateway.config.JwtConfigProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication()
@EnableConfigurationProperties(JwtConfigProperties.class)
public class Meal0ApiGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(Meal0ApiGatewayApplication.class, args);
    }
}
