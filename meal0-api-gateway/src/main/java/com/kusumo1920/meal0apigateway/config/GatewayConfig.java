package com.kusumo1920.meal0apigateway.config;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableHystrix
@RequiredArgsConstructor
public class GatewayConfig {

    private final AuthenticationFilter filter;

//    @Bean
//    public RouteLocator routes(RouteLocatorBuilder builder) {
////        return builder.routes()
////                .route("meal0-inventory", r -> r.path("/api/v1/inventory/**")
////                        .filters(f -> f.filter(filter))
////                        .uri("lb://meal0-inventory"))
////
////                .route("meal0-auth", r -> r.path("/api/v1/auth/**")
////                        .filters(f -> f.filter(filter))
////                        .uri("lb://meal0-user"))
////
////                .route("meal0-user", r -> r.path("/api/v1/user/**")
////                        .filters(f -> f.filter(filter))
////                        .uri("lb://meal0-user"))
////
////                .route("meal0-order", r -> r.path("/api/v1/order/**")
////                        .filters(f -> f.filter(filter))
////                        .uri("lb://meal0-order"))
////
////                .route("meal0-food", r -> r.path("/api/v1/food/**")
////                        .filters(f -> f.filter(filter))
////                        .uri("lb://meal0-food"))
////                .build();
//    }

}
