package com.kusumo1920.meal0apigateway.config;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Predicate;

@Component
public class RouterValidator {
    public static final List<String> unsecuredApiEndpoints = List.of(
            "/api/v1/auth"
    );

    public Predicate<ServerHttpRequest> isSecured =
            serverHttpRequest -> unsecuredApiEndpoints
                    .stream()
                    .noneMatch(uri -> serverHttpRequest.getURI().getPath().contains(uri));
}
