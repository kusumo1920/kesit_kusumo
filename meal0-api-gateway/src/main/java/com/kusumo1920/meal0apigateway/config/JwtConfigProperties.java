package com.kusumo1920.meal0apigateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("jwtconfig")
public record JwtConfigProperties(String secretKey) {}
