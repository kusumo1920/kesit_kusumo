# Meal0 - Cheaper Food, Reduce Waste!
----

## Diagram Arsitektur

![meal0-microservices-diagram.png](meal0-microservices-diagram.png)

* Link diagram (jika tidak muncul): https://drive.google.com/file/d/1fm2CNPS_p8Oa9p2T8vl-OrlRs8Ls9GQ3/view?usp=sharing

## Update Code
![log-scr.png](log-scr.png)
#### Screenshot log balikan response dari external API https://jsonplaceholder.typicode.com/posts 

![service-scr.png](service-scr.png)
#### Screenshot code untuk Service.

![controller-scr.png](controller-scr.png)
#### Screenshot code untuk Controller.

![postman-scr.png](postman-scr.png)
#### Screenshot code untuk Postman.

### Flow
- User kirim POST data.
- Order Service kirim data ke external API jsonplaceholder.
- Kirim data berupa response balikan dari jsonplaceholder kembali ke user. 

##### Deskripsi:
* Dalam sistem ini, terdapat 6 microservices berbeda yang memiliki fungsionalitas masing-masing.
Microservices tersebut adalah:

### API Gateway
* Berfungsi dalam menerima semua request dari client (PC, mobile phone, dll). 
* Semua request harus melalui API gateway ini.

### Auth/User Service
* Berfungsi meng-autentikasi user.
* Apabila request ditujukan untuk mengakses akses private/rahasia, maka API gateway service akan bekerja sama dengan service ini.
* Apabila request ditujukan untuk mengakses akses publik/bebas, maka API gateway akan meneruskan request tersebut ke service yang ingin dituju.

### Eureka Service
* Berfungsi untuk mencari (discover) dan mendaftar microservices yang aktif.
* Hal ini berguna ketika microservice ingin melakukan komunikasi dengan microservice lain.

### Food Service
* Berfungsi untuk menyediakan layanan yang berkaitan dengan makanan.
* Memiliki koneksi dengan MongoDB (NoSQL Database).

### Order Service
* Berfungsi untuk menyediakan layanan yang berkaitan dengan order/pesanan.
* Memiliki koneksi dengan Postgres (SQL Database).
* Memiliki koneksi dengan Inventory Service untuk mengecek ketersediaan stok ketika membuat pesanan.

### Inventory Service
* Berfungsi untuk menyediakan layanan yang berkaitan dengan inventory makanan.
* Memiliki koneksi dengan Postgres (SQL Database).

----

## Demo Live (Hosted in DigitalOcean)
* IP address: 157.230.248.114
* Link to eureka (discover server): http://157.230.248.114:8761/
* Api Gateway: http://157.230.248.114:8084
* Auth/User Service: http://157.230.248.114:8080
* Food Service: http://157.230.248.114:8081
* Order Service: http://157.230.248.114:8082
* Inventory Service: http://157.230.248.114:8083

## Demo Lokal (IDE) & docker compose
* Untuk demo di IDE, bisa dijalankan setiap module app sebagai Java Application (contoh: di IntelliJ Idea).
* Untuk demo dengan docker compose, di root folder, jalankan perintah di terminal
```
docker compose up -d
```
* Untuk docker compose, silahkan tunggu waktu beberapa menit, karena butuh waktu proses inisialisasi termasuk discovery microservices.
* Jika menjalankan docker compose dilakukan, host yang dipakai adalah localhost dengan port yang sama seperti di demo live atas.

----

## API

### User API
1. Register user
- PUBLIC (No Need Bearer Token in Authorization header)
- POST: /api/v1/auth/register
- Request Body Sample:
```json
{
  "firstName": "Jimbe",
  "lastName": "OP",
  "email": "jimbe@email.com",
  "password": "1234"
}
```
- Response sample:
```json
{
    "code": 201,
    "message": "Successfully registered user.",
    "data": {
        "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJrdXN1bW9idWRpQGVtYWlsLmNvbSIsImlhdCI6MTY5MTg1NDIwNSwiZXhwIjoxNjkxODU1NjQ1fQ.e00tVcJsTrxaCalduoPBmR_PWxP3j1afcnfiqtniGdA"
    }
}
```

2. Authenticate user
- PUBLIC (No Need Bearer Token in Authorization header)
- POST: /api/v1/auth/authenticate
- Request Body Sample:
```json
{
    "email": "jimbe@email.com",
    "password": "1234"
}
```
- Response sample:
```json
{
  "code": 200,
  "message": "Successfully logged in.",
  "data": {
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJrdXN1bW9idWRpQGVtYWlsLmNvbSIsImlhdCI6MTY5MTg1NDIyOSwiZXhwIjoxNjkxODU1NjY5fQ.SpL-gu8uLvSCD64Lk6Ol9Qqr90QjXuiReehztxha4E4"
  }
}
```

3. Check token validity
- PUBLIC (No Need Bearer Token in Authorization header)
- POST: /api/v1/auth/check-token-validity
- Request Body Sample:
```json
{
  "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqaW1iZUBlbWFpbC5jb20iLCJpYXQiOjE2OTE4MTM0NjQsImV4cCI6MTY5MTgxNDkwNH0.ycVFyffqUnIgnzj5rcohOqmGO44KJ8hk83Xp3MHQJw8"
}
```
- Response sample:
```json
{
    "code": 200,
    "message": "Successfully checked token.",
    "data": {
        "username": "jimbe@email.com",
        "role": [
            {
                "authority": "CUSTOMER"
            }
        ],
        "valid": true
    }
}
```

4. Get user profile
- PRIVATE (Need Bearer Token in Authorization header)
- GET: /api/v1/user/profile
- Response sample
```json
{
    "code": 200,
    "message": "Successfully get user profile.",
    "data": {
        "firstName": "jimbe",
        "lastName": "jimbe",
        "email": "jimbe@email.com"
    }
}
```

----

### Food API

1. Create food
- PRIVATE (Need Bearer Token in Authorization header)
- POST: /api/v1/food
- Request Body Sample:
```json
{
  "name": "Bubur Ayam",
  "description": "Bubur Ayam",
  "price": "20000",
  "skuCode": "bubur_ayam"
}
```
- Response sample:
```json
{
    "code": 201,
    "message": "Successfully created food.",
    "data": null
}
```

2. Get all foods
- PRIVATE (Need Bearer Token in Authorization header)
- GET: /api/v1/food
- Response sample:
```json
{
    "code": 200,
    "message": "Successfully get all foods.",
    "data": [
        {
            "id": "64d5f368453600351f83087f",
            "name": "Rujak",
            "description": "Rujak",
            "price": 10000,
            "skuCode": null
        }
    ]
}
```

----

### Order API

1. Place new order
- PRIVATE (Need Bearer Token in Authorization header)
- POST: /api/v1/order
- Request Body Sample:
```json
{
  "orderLineItemsDataList": [
    {
      "skuCode": "bubur_ayam",
      "price": 20000,
      "qty": 3
    },
    {
      "skuCode": "nasi_goreng",
      "price": 15000,
      "qty": 4
    }
  ]
}
```
- Response sample:
```json
{
    "code": 201,
    "message": "Successfully placed order.",
    "data": null
}
```

2. Get order by id
- PRIVATE (Need Bearer Token in Authorization header)
- GET: /api/v1/order/2
- Response sample:
```json
{
    "code": 200,
    "message": "Successfully get order.",
    "data": {
        "id": 2,
        "orderNumber": "5a6b2dba-2826-4534-bacd-33ac9d7fc3d5",
        "orderLineItemsList": [
            {
                "id": 2,
                "skuCode": "bubur_ayam",
                "price": 20000.00,
                "qty": 3
            },
            {
                "id": 3,
                "skuCode": "nasi_goreng",
                "price": 15000.00,
                "qty": 4
            }
        ],
        "customerUsername": "jimbe@email.com"
    }
}
```

----

### Inventory Service

1. Check stock
- PRIVATE (Need Bearer Token in Authorization header)
- GET: api/v1/inventory/check-stock?skuCode=rujak_manis
- Response sample:
```json
{
    "code": 200,
    "message": "Successfully checked inventory stock.",
    "data": [
        {
            "skuCode": "rujak_manis",
            "inStock": true
        }
    ]
}
```


----
Author:
Kesit Budi Kusumo.
