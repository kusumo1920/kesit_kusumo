package com.kusumo1920.meal0user.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("jwtconfig")
public record JwtConfigProperties(String secretKey) {
}
