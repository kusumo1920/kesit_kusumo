package com.kusumo1920.meal0user;

import com.kusumo1920.meal0user.config.JwtConfigProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(JwtConfigProperties.class)
public class Meal0Application {

	public static void main(String[] args) {
		SpringApplication.run(Meal0Application.class, args);
	}

}
