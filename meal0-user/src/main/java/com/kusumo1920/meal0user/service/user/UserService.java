package com.kusumo1920.meal0user.service.user;

import com.kusumo1920.meal0user.controller.user.UserProfileResponseData;
import com.kusumo1920.meal0user.dao.user.User;
import com.kusumo1920.meal0user.dao.user.UserRepository;
import com.kusumo1920.meal0user.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public UserProfileResponseData getSelfUserProfile(String username) {
        Optional<User> optionalUser = userRepository.findByUsername(username);

        if (optionalUser.isEmpty()) {
            throw new NotFoundException("User is not found.");
        }

        User user = optionalUser.get();

        return UserProfileResponseData.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .build();
    }
}
