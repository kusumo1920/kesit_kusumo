package com.kusumo1920.meal0user.service.auth;

import com.kusumo1920.meal0user.controller.auth.*;
import com.kusumo1920.meal0user.dao.user.Role;
import com.kusumo1920.meal0user.dao.user.User;
import com.kusumo1920.meal0user.dao.user.UserRepository;
import com.kusumo1920.meal0user.exception.AlreadyRegisteredException;
import com.kusumo1920.meal0user.exception.NotFoundException;
import com.kusumo1920.meal0user.exception.UnauthorizedException;
import io.jsonwebtoken.security.SignatureException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;

    public AuthenticationResponsePayload register(RegisterRequest request) {
        var user = User.builder()
                .firstName(request.getFirstName().trim())
                .lastName(request.getLastName().trim())
                .email(request.getEmail().trim().toLowerCase())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.CUSTOMER)
                .build();

        var userInDb = userRepository.findByEmail(request.getEmail().trim().toLowerCase());

        if (userInDb.isPresent()) {
            throw new AlreadyRegisteredException("User is already registered previously.");
        }

        userRepository.save(user);

        var jwtToken = jwtService.generateToken(user);

        AuthenticationResponseData responseData = AuthenticationResponseData
                .builder()
                .token(jwtToken)
                .build();

        return AuthenticationResponsePayload
                .builder()
                .code(201)
                .message("Successfully registered user.")
                .data(responseData)
                .build();
    }

    public AuthenticationResponsePayload authenticate(AuthenticationRequest request) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getEmail(),
                            request.getPassword()
                    )
            );
        } catch (AuthenticationException e) {
            throw new UnauthorizedException("Unauthorized, username/password is incorrect.");
        }


        var user = userRepository.findByEmail(request.getEmail());

        if (user.isEmpty()) {
            throw new UnauthorizedException("Unauthorized, user is not found.");
        }

        var jwtToken = jwtService.generateToken(user.get());

        AuthenticationResponseData responseData = AuthenticationResponseData
                .builder()
                .token(jwtToken)
                .build();

        return AuthenticationResponsePayload
                .builder()
                .code(200)
                .message("Successfully logged in.")
                .data(responseData)
                .build();
    }

    public CheckTokenValidityResponseData checkTokenValidity(String token) {
        boolean isTokenAvailable = token != null;

        CheckTokenValidityResponseData invalidResponseData = CheckTokenValidityResponseData
                .builder()
                .isValid(false)
                .username(null)
                .role(null)
                .build();

        if (!isTokenAvailable) {
            return invalidResponseData;
        }

        try {
            final String userEmail = jwtService.extractUsername(token);

            if (userEmail != null) {
                UserDetails userDetails = this.userDetailsService.loadUserByUsername(userEmail);

                if (jwtService.isTokenValid(token, userDetails)) {
                    return CheckTokenValidityResponseData
                            .builder()
                            .isValid(true)
                            .username(userEmail)
                            .role(userDetails.getAuthorities())
                            .build();
                }
            }
        } catch (SignatureException e) {
            return invalidResponseData;
        }

        return invalidResponseData;
    }
}
