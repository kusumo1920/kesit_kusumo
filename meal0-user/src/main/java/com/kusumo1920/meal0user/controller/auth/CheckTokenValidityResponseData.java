package com.kusumo1920.meal0user.controller.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CheckTokenValidityResponseData {
    private boolean isValid;
    private String username;
    private Collection<? extends GrantedAuthority> role;
}
