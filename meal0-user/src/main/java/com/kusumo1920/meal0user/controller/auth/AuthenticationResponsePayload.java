package com.kusumo1920.meal0user.controller.auth;

import com.kusumo1920.meal0user.controller.common.ResponseWrapper;
import lombok.*;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationResponsePayload extends ResponseWrapper {
    private AuthenticationResponseData data;
}
