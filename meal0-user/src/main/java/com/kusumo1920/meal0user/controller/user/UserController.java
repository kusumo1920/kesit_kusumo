package com.kusumo1920.meal0user.controller.user;

import com.kusumo1920.meal0user.service.user.UserService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/profile")
    public ResponseEntity<UserProfileResponsePayload> getSelfUserProfile(
            HttpServletRequest httpServletRequest
    ) {
        String username = httpServletRequest.getHeader("username");

        UserProfileResponseData responseData = userService.getSelfUserProfile(username);

        UserProfileResponsePayload responsePayload = UserProfileResponsePayload.builder()
                .data(responseData)
                .code(HttpStatus.OK.value())
                .message("Successfully get user profile.")
                .build();

        return ResponseEntity.ok(responsePayload);
    }
}
