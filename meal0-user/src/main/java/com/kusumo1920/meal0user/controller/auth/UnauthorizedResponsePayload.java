package com.kusumo1920.meal0user.controller.auth;

import com.kusumo1920.meal0user.controller.common.ResponseWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class UnauthorizedResponsePayload extends ResponseWrapper {
    private Object data;
}
