package com.kusumo1920.meal0user.controller.auth;

import com.kusumo1920.meal0user.service.auth.AuthenticationService;
import com.kusumo1920.meal0user.service.auth.JwtService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationService authenticationService;

    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponsePayload> register(
            @RequestBody RegisterRequest request
    ) {
        return ResponseEntity.ok(authenticationService.register(request));
    }

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponsePayload> authenticate(
            @RequestBody AuthenticationRequest request
    ) {
        return ResponseEntity.ok(authenticationService.authenticate(request));
    }

    @PostMapping("/check-token-validity")
    public ResponseEntity<CheckTokenValidityResponsePayload> checkTokenValidity(
            @RequestBody CheckTokenValidityRequestBody requestBody
    ) {
        CheckTokenValidityResponseData responseData = authenticationService.checkTokenValidity(requestBody.getToken());

        CheckTokenValidityResponsePayload responsePayload = CheckTokenValidityResponsePayload
                .builder()
                .code(200)
                .message("Successfully checked token.")
                .data(responseData)
                .build();

        return ResponseEntity.ok(responsePayload);

    }
}
