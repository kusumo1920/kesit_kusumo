package com.kusumo1920.meal0user.controller.auth;

import com.kusumo1920.meal0user.exception.AlreadyRegisteredException;
import com.kusumo1920.meal0user.exception.UnauthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AuthExceptionHandler {
    @ExceptionHandler
    public ResponseEntity<UnauthorizedResponsePayload> handleException(UnauthorizedException exception) {
        UnauthorizedResponsePayload responsePayload = UnauthorizedResponsePayload.builder()
                .data(null)
                .code(HttpStatus.UNAUTHORIZED.value())
                .message(exception.getMessage())
                .build();

        return new ResponseEntity<>(responsePayload, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler
    public ResponseEntity<AlreadyRegisteredResponsePayload> handleException(AlreadyRegisteredException exception) {
        AlreadyRegisteredResponsePayload responsePayload = AlreadyRegisteredResponsePayload.builder()
                .data(null)
                .code(HttpStatus.BAD_REQUEST.value())
                .message(exception.getMessage())
                .build();

        return new ResponseEntity<>(responsePayload, HttpStatus.BAD_GATEWAY);
    }
}
