package com.kusumo1920.meal0user.controller.user;

import com.kusumo1920.meal0user.controller.auth.NotFoundResponsePayload;
import com.kusumo1920.meal0user.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UserExceptionHandler {
    @ExceptionHandler
    public ResponseEntity<NotFoundResponsePayload> handleException(NotFoundException exception) {
        NotFoundResponsePayload responsePayload = NotFoundResponsePayload.builder()
                .data(null)
                .code(HttpStatus.NOT_FOUND.value())
                .message(exception.getMessage())
                .build();

        return new ResponseEntity<>(responsePayload, HttpStatus.NOT_FOUND);
    }
}
