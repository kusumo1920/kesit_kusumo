package com.kusumo1920.meal0user.dao.user;

public enum Role {
    CUSTOMER,
    ADMIN
}
