package com.kusumo1920.meal0order.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class OrderLineItemsData {
    private Integer id;
    private String skuCode;
    private BigDecimal price;
    private Integer qty;
}
