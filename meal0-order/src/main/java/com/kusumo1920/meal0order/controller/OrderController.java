package com.kusumo1920.meal0order.controller;

import com.kusumo1920.meal0order.dao.Order;
import com.kusumo1920.meal0order.service.OrderService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/order")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<PlaceOrderResponsePayload> placeOrder(
            @RequestBody OrderRequest orderRequest,
            HttpServletRequest httpServletRequest
    ) {
        final String username = httpServletRequest.getHeader("username");

        orderService.placeOrder(orderRequest, username);

        PlaceOrderResponsePayload responsePayload = PlaceOrderResponsePayload.builder()
                .code(201)
                .message("Successfully placed order.")
                .data(null)
                .build();

        return new ResponseEntity<>(responsePayload, HttpStatus.CREATED);
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<GetOrderResponsePayload> getOrderById(
            @PathVariable int orderId
    ) {
        Order order = orderService.findOrderById(orderId);

        GetOrderResponsePayload responsePayload = GetOrderResponsePayload.builder()
                .data(order)
                .code(HttpStatus.OK.value())
                .message("Successfully get order.")
                .build();

        return ResponseEntity.ok(responsePayload);
    }
}
