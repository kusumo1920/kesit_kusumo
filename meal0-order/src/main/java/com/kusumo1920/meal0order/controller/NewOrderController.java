package com.kusumo1920.meal0order.controller;

import com.kusumo1920.meal0order.service.NewOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/order/new")
@RequiredArgsConstructor
public class NewOrderController {
    private final NewOrderService newOrderService;

    @PostMapping
    public ResponseEntity<TodoResponsePayload> process(
            @RequestBody TodoRequest reqData
    ) {
        TodoResponseData responseData = newOrderService.process(reqData);

        TodoResponsePayload responsePayload = TodoResponsePayload.builder()
                .data(responseData)
                .code(200)
                .message("Successful")
                .build();

        return ResponseEntity.ok(responsePayload);
    }
}
