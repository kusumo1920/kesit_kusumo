package com.kusumo1920.meal0order.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class TodoResponseData {
    private int userId;
    private int id;
    private String title;
    private String body;
}
