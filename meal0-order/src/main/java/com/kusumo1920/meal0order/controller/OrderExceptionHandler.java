package com.kusumo1920.meal0order.controller;

import com.kusumo1920.meal0order.exception.NotFoundException;
import com.kusumo1920.meal0order.exception.OutOfStockException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class OrderExceptionHandler {
    @ExceptionHandler
    public ResponseEntity<NotFoundResponsePayload> handleException(NotFoundException exception) {
        NotFoundResponsePayload responsePayload = NotFoundResponsePayload.builder()
                .data(null)
                .code(HttpStatus.NOT_FOUND.value())
                .message(exception.getMessage())
                .build();

        return new ResponseEntity<>(responsePayload, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<OutOfStockResponsePayload> handleException(OutOfStockException exception) {
        OutOfStockResponsePayload responsePayload = OutOfStockResponsePayload.builder()
                .data(null)
                .code(HttpStatus.BAD_REQUEST.value())
                .message(exception.getMessage())
                .build();

        return new ResponseEntity<>(responsePayload, HttpStatus.BAD_REQUEST);
    }
}
