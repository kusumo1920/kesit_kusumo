package com.kusumo1920.meal0order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Meal0OrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(Meal0OrderApplication.class, args);
	}

}
