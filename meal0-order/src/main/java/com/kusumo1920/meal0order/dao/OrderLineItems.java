package com.kusumo1920.meal0order.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "order_line_item")
public class OrderLineItems implements Serializable {
    @Id
    @GeneratedValue
    private Integer id;
    private String skuCode;
    private BigDecimal price;
    private Integer qty;
    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    @JsonIgnore
    private Order order;
}
