package com.kusumo1920.meal0order.service;

import com.kusumo1920.meal0order.controller.TodoRequest;
import com.kusumo1920.meal0order.controller.TodoResponseData;
import com.kusumo1920.meal0order.controller.TodoResponsePayload;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;


@Service
@RequiredArgsConstructor
@Transactional
public class NewOrderService {
    private final WebClient.Builder webClient;
    Logger logger = LoggerFactory.getLogger(NewOrderService.class);

    public TodoResponseData process(TodoRequest data) {
        TodoResponseData outsideResponse = webClient.build().post()
                .uri("https://jsonplaceholder.typicode.com/posts")
                .bodyValue(data)
                .retrieve()
                .bodyToMono(TodoResponseData.class)
                .block();

        logger.info("Following response data from external APIs (https://jsonplaceholder.typicode.com/posts) :");
        logger.info(String.valueOf(outsideResponse));

        return outsideResponse;
    }
}
