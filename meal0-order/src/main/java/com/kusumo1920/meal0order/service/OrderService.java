package com.kusumo1920.meal0order.service;

import com.kusumo1920.meal0order.controller.InventoryResponseItem;
import com.kusumo1920.meal0order.controller.IsInStockResponsePayload;
import com.kusumo1920.meal0order.controller.OrderLineItemsData;
import com.kusumo1920.meal0order.controller.OrderRequest;
import com.kusumo1920.meal0order.dao.Order;
import com.kusumo1920.meal0order.dao.OrderLineItems;
import com.kusumo1920.meal0order.dao.OrderRepository;
import com.kusumo1920.meal0order.exception.NotFoundException;
import com.kusumo1920.meal0order.exception.OutOfStockException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderService {
    private final OrderRepository orderRepository;
    private final WebClient.Builder webClientBuilder;

    public void placeOrder(OrderRequest orderRequest, String username) {
        Order order = Order.builder()
                .orderNumber(UUID.randomUUID().toString())
                .customerUsername(username)
                .build();

        List<OrderLineItems> orderLineItems = orderRequest.getOrderLineItemsDataList()
                .stream()
                .map(item -> mapToDao(item, order))
                .toList();

        order.setOrderLineItemsList(orderLineItems);

        List<String> skuCodes = order.getOrderLineItemsList().stream()
                .map(OrderLineItems::getSkuCode)
                .toList();

        IsInStockResponsePayload inventoryResponsePayload = webClientBuilder.build().get()
                .uri("http://meal0-inventory/api/v1/inventory/check-stock",
                        uriBuilder -> uriBuilder.queryParam("skuCode", skuCodes).build())
                .retrieve()
                .bodyToMono(IsInStockResponsePayload.class)
                .block();

        boolean allProductsInStock = inventoryResponsePayload.getData().stream()
                .allMatch(InventoryResponseItem::isInStock);

        if (allProductsInStock) {
            orderRepository.save(order);
        } else {
            throw new OutOfStockException("Some/all food are out of stock.");
        }
    }

    private OrderLineItems mapToDao(OrderLineItemsData orderLineItemsData, Order order) {
        return OrderLineItems.builder()
                .price(orderLineItemsData.getPrice())
                .qty(orderLineItemsData.getQty())
                .skuCode(orderLineItemsData.getSkuCode())
                .order(order)
                .build();
    }

    public Order findOrderById(int orderId) {
        Optional<Order> optionalOrder = orderRepository.findById(orderId);

        Order order = optionalOrder.orElse(null);

        if (order == null) {
            throw new NotFoundException("Order is not found.");
        }

        return order;
    }
}
