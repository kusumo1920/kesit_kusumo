package com.kusumo1920.meal0food.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FoodResponseData {
    private String id;
    private String name;
    private String description;
    private BigDecimal price;
    private String skuCode;
}
