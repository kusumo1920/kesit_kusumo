package com.kusumo1920.meal0food.controller;

import com.kusumo1920.meal0food.controller.common.ResponseWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class GetAllFoodsResponsePayload extends ResponseWrapper {
    private List<FoodResponseData> data;
}
