package com.kusumo1920.meal0food.controller;

import com.kusumo1920.meal0food.service.FoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/food")
@RequiredArgsConstructor
public class FoodController {
    private final FoodService foodService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CreateFoodResponsePayload> createFood(@RequestBody FoodRequest foodRequest) {
        foodService.createFood(foodRequest);

        CreateFoodResponsePayload responsePayload = CreateFoodResponsePayload.builder()
                .code(201)
                .message("Successfully created food.")
                .data(null)
                .build();

        return new ResponseEntity<>(responsePayload, HttpStatus.CREATED);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<GetAllFoodsResponsePayload> getAllFoods() {
        List<FoodResponseData> allFoods = foodService.getAllFoods();

        GetAllFoodsResponsePayload responsePayload = GetAllFoodsResponsePayload.builder()
                .code(200)
                .message("Successfully get all foods.")
                .data(allFoods)
                .build();

        return ResponseEntity.ok(responsePayload);
    }
}
