package com.kusumo1920.meal0food.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Document(value = "food")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Food {
    @Id
    private String id;
    private String name;
    private String description;
    private BigDecimal price;
    private String skuCode;
}
