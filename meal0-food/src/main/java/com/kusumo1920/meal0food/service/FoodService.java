package com.kusumo1920.meal0food.service;

import com.kusumo1920.meal0food.controller.FoodRequest;
import com.kusumo1920.meal0food.controller.FoodResponseData;
import com.kusumo1920.meal0food.dao.Food;
import com.kusumo1920.meal0food.dao.FoodRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class FoodService {
    private final FoodRepository foodRepository;

    public void createFood(FoodRequest foodRequest) {
        Food food = Food.builder()
                .name(foodRequest.getName())
                .description(foodRequest.getDescription())
                .price(foodRequest.getPrice())
                .skuCode(foodRequest.getSkuCode())
                .build();

        foodRepository.save(food);
        log.info("Food {} is saved.", food.getId());
    }

    public List<FoodResponseData> getAllFoods() {
        List<Food> foods = foodRepository.findAll();

        return foods.stream().map(this::mapToFoodResponseData).toList();
    }

    private FoodResponseData mapToFoodResponseData(Food food) {
        return FoodResponseData.builder()
                .id(food.getId())
                .name(food.getName())
                .description(food.getDescription())
                .price(food.getPrice())
                .skuCode(food.getSkuCode())
                .build();
    }
}
