package com.kusumo1920.meal0food;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Meal0FoodApplication {

	public static void main(String[] args) {
		SpringApplication.run(Meal0FoodApplication.class, args);
	}

}
