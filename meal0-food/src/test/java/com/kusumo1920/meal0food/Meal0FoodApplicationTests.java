package com.kusumo1920.meal0food;

import com.kusumo1920.meal0food.controller.FoodRequest;
import com.kusumo1920.meal0food.dao.FoodRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
class Meal0FoodApplicationTests {
	@Container
	static MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:4.4.2");

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private FoodRepository foodRepository;

	@DynamicPropertySource
	static void setProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
		dynamicPropertyRegistry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
	}

	@Test
	void shouldCreateFood() throws Exception {
		FoodRequest foodRequest = getFoodRequest();
		String foodRequestString = objectMapper.writeValueAsString(foodRequest);

		mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/food")
				.contentType(MediaType.APPLICATION_JSON)
				.content(foodRequestString))
				.andExpect(status().isCreated());

		Assertions.assertEquals(1, foodRepository.findAll().size());
	}

	private FoodRequest getFoodRequest() {
		return FoodRequest.builder()
				.name("Rujak")
				.description("Rujak")
				.price(BigDecimal.valueOf(10000))
				.build();
	}

}
